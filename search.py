#!/usr/bin/env python
# _*_ coding:utf-8 _*_
"""
@ProjectName:baidu-search-com
@FileName   :search.py
@CreateTime :2024/4/25 10:57
@Author     : lurker
@E-mail     : 289735192@qq.com
@Desc.      :
"""

import configparser
import codecs
import asyncio
import os

import openpyxl
from fake_useragent import UserAgent
from pyppeteer import launcher, launch
import logging

import requests

chromium_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'chrome-win', 'chrome')

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s - %(message)s')

launcher.DEFAULT_ARGS.remove("--enable-automation")


class LogColor:
    """
    根据不同的日志级别，打印不颜色的日志
    info：绿色
    warning：黄色
    error：红色
    debug：灰色
    """
    # logging日志格式设置
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s - %(levelname)s: %(message)s')

    @staticmethod
    def info(message: str):
        # info级别的日志，绿色
        logging.info("\033[0;32m" + message + "\033[0m")

    @staticmethod
    def warning(message: str):
        # warning级别的日志，黄色
        logging.warning("\033[0;33m" + message + "\033[0m")

    @staticmethod
    def error(message: str):
        # error级别的日志，红色
        logging.error("\033[0;31m" + "-" * 120 + '\n| ' + message + "\033[0m" + "\n" + "└" + "-" * 150)

    @staticmethod
    def debug(message: str):
        # debug级别的日志，灰色
        logging.debug("\033[0;37m" + message + "\033[0m")


def screen_size():
    """使用tkinter获取屏幕大小"""
    import tkinter
    tk = tkinter.Tk()
    width = tk.winfo_screenwidth()
    height = tk.winfo_screenheight()
    tk.quit()
    return width, height


def get_user_agent():
    user_agent = UserAgent()
    return str(user_agent.random)


def get_proxy():
    try:
        response = requests.get(PROXY_API_URL)
        txt = response.text
        if response.status_code == 200:
            ip = txt.strip()
            LogColor.info(f'获取IP代理成功：{ip}')
        else:
            LogColor.warning('获取代理IP失败')
            ip = ''
        return ip
    except Exception as e:
        LogColor.error('获取代理IP发生错误:' + str(e))
        return ''


def write_record(file_name, s):
    with open(file_name, 'a+', encoding='utf-8') as f:
        f.write(s)
    return None


async def page_evaluate(page):
    await page.evaluate('''() =>{ Object.defineProperties(navigator,{ webdriver:{ get: () => undefined } }) }''')
    await page.evaluate('''() =>{ window.navigator.chrome = { runtime: {},  }; }''')
    await page.evaluate('''() =>{ Object.defineProperty(navigator, 'languages', { get: () => ['en-US', 'en'] }); }''')
    await page.evaluate('''() =>{ Object.defineProperty(navigator, 'plugins', { get: () => [1, 2, 3, 4, 5,6], }); }''')


async def main():
    # 判断搜索平台
    if platform == 1:
        while True:
            # print(kws)
            if len(kws) == 0:
                LogColor.warning('操作完成!!!')
                await asyncio.sleep(500)
                break
            kw = kws.pop(0)
            logging.info(f'开始搜索：【{kw[1]}】')
            try:
                # 获取代理IP
                proxy = get_proxy()
                user_agent = ua.chrome
                if not proxy:
                    LogColor.warning('获取代理IP失败')
                    return 0
                browser = await launch(
                    {"executablePath": chromium_path, "headless": False, "dumpio": True, "autoClose": False,
                     "userDataDir": "./userdata",
                     "args": ["–-no-sandbox", "--disable-infobars", f'--user-agent={user_agent}',
                              f'--proxy-server=http://{proxy}']})

                # 创建一个页面对象， 页面操作在该对象上执行
                page = await browser.newPage()
                width, height = screen_size()
                await page.setViewport({'width': width, 'height': height - 100})
                await page_evaluate(page)
                operate_result = await execute_task(page, kw[1])
                if operate_result == 0:
                    kws.append(kw)
                elif operate_result == 1:
                    # record_line = f'{kw[0]} {kw[1]}\n'
                    record_line = f'{kw[0]}\n'
                    write_record(output_file, record_line)
                    LogColor.info(f'搜索【{kw[1]}】符合条件，写入【{output_file}】')
                elif operate_result == 2:
                    LogColor.info(f'搜索【{kw[1]}】不符合条件！')
                await page.close()
                await browser.close()
            except Exception as e:
                LogColor.error(f'搜索{kw[1]}时发生错误！：{str(e)}')
                kws.append(kw)
    elif platform == 2:
        # 搜狗
        LogColor.warning('暂不支持搜狗')
    elif platform == 3:
        # 必应
        LogColor.warning('暂时不支持必应')
    else:
        LogColor.warning('未设置搜索平台')


def match_result(_condition_rlt, word, c, flag):
    if flag == 'and':
        res = 1
        LogColor.info(f'搜索【{word}】至第【{c}】页：')
        for k, v in _condition_rlt.items():
            LogColor.info(f'{k}:{v}')
            if v == 0:
                res = 0
        return res
    elif flag == 'or':
        res = 0
        LogColor.info(f'搜索【{word}】至第【{c}】页：')
        for k, v in _condition_rlt.items():
            LogColor.info(f'{k}:{v}')
            if v == 1:
                res = 1
        return res
    else:
        res = 0
        LogColor.info(f'搜索【{word}】至第【{c}】页：')
        for k, v in _condition_rlt.items():
            LogColor.info(f'{k}:{v}')
            if v == 1:
                res = 1
        return res


async def execute_task(p, d):
    # 百度
    condition_rlt = {}
    for i in condition:
        condition_rlt[i] = 0
    counter = 0
    try:
        # await p.goto('https://www.baidu.com/', options={'waitUntil': 'networkidle0'})
        await p.goto('https://www.baidu.com/')
        await p.waitForSelector('#kw')
        await p.type('#kw', d)
        await p.waitForSelector('#su')
        await p.click('#su')
        await asyncio.sleep(stay / 1000)
        # items = await p.querySelectorAll('#content_left>div[tpl="se_com_default"]')
        items = await p.querySelectorAll('#content_left>div')
        for item in items:
            text = await p.evaluate('(element) => element.textContent', item)
            # print(text)
            if d in text:
                for k in condition_rlt.keys():
                    if k in text:
                        condition_rlt[k] = 1
                    # if '百度百科' in text:
                    #     condition_rlt['百度百科'] = 1
                    # elif '爱番番' in text:
                    #     condition_rlt['爱番番'] = 1
                    # elif '百度爱采购' in text:
                    #     condition_rlt['百度爱采购'] = 1
        counter += 1
        if match_result(condition_rlt, d, counter, match_flag):
            cookies = await p.cookies()
            for cookie in cookies:
                await p.deleteCookie(cookie)
            # 符合条件
            return 1

        # 获取分页链接
        a_links_tag = await p.querySelectorAll('#page > div >a')
        a_links = []
        for a_link_tag in a_links_tag[:-1][:pages - 1]:
            a_link = await p.evaluate('item => item.href', a_link_tag)
            a_links.append(a_link)
        for a_link in a_links:
            await p.goto(a_link)
            await p.waitForSelector('#content_left')
            await asyncio.sleep(stay / 1000)
            # items = await p.querySelectorAll('#content_left>div[tpl="se_com_default"]')
            items = await p.querySelectorAll('#content_left>div')
            for item in items:
                text = await p.evaluate('(element) => element.textContent', item)
                # print(text)
                if d in text:
                    for k in condition_rlt.keys():
                        if k in text:
                            condition_rlt[k] = 1
                        # if '百度百科' in text:
                        #     condition_rlt['百度百科'] = 1
                        # elif '爱番番' in text:
                        #     condition_rlt['爱番番'] = 1
                        # elif '百度爱采购' in text:
                        #     condition_rlt['百度爱采购'] = 1
            counter += 1
            if match_result(condition_rlt, d, counter, match_flag):
                cookies = await p.cookies()
                for cookie in cookies:
                    await p.deleteCookie(cookie)
                # 符合条件
                return 1
        return 2
    except Exception as e:
        LogColor.error(f'搜索【{d}】发生错误：{str(e)}')
        # 操作出错
        return 0
    finally:
        # 清除所有cookies
        cookies = await p.cookies()
        for cookie in cookies:
            await p.deleteCookie(cookie)


if __name__ == '__main__':
    # 读配置文件
    config = configparser.ConfigParser()
    config.read_file(codecs.open('conf.ini', "r", "utf-8-sig"))
    # platform = int(config.get('settings', 'platform'))
    # 预留
    platform = 1
    # 操作间隔
    stay = int(config.get('settings', 'stay'))
    # 代理IP接口
    PROXY_API_URL = config.get('settings', 'api')
    # 公司数据文件
    keys_file = config.get('settings', 'file')
    # 结果保存文件
    output_file = config.get('settings', 'output')
    # 查找页数
    pages = int(config.get('settings', 'pages'))
    # 匹配条件
    condition_str = config.get('settings', 'match')
    if 'or' in condition_str:
        match_flag = 'or'
        condition = condition_str.split('or')
    elif 'and' in condition_str:
        match_flag = 'and'
        condition = condition_str.split('and')
    else:
        match_flag = 0
        condition = condition_str.split()
    # 读取关键词
    kw_book = openpyxl.load_workbook(keys_file, data_only=True)
    kw_sheet = kw_book['Sheet1']
    rows = kw_sheet.rows
    # file_name = 'result.txt'
    keys = []
    for row in rows:
        keys.append((row[0].value, row[1].value))
    kws = keys[1:]
    ua = UserAgent(os=['windows'])
    asyncio.run(main())
